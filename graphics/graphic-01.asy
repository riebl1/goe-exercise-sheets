import graph3;

size(10cm);

draw((0,0,1)--(1,0,0)--(0,1,0)--cycle);
draw((0.4,0,0.3)--(0.4,0.3,0.3)--(0.4,0.3,0)--(0.4,0,0)--cycle,dashed);
draw((0.4,0,0.3)--(0,0,0.3)--(0,0.3,0.3)--(0.4,0.3,0.3),dashed);
draw((0,0.3,0.3)--(0,0.3,0)--(0.4,0.3,0),dashed);

dot((0.4,0.3,0.3),red);
label("$P$",(0.4,0.3,0.3),SW);

xaxis3("$x$",0,1.2,InOutTicks(3,2),Arrow3);
yaxis3("$y$",0,1.2,InOutTicks(3,2),Arrow3);
zaxis3("$z$",0,1.2,InOutTicks(3,2),Arrow3);
